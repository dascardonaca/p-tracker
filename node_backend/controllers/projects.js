// Importing Project model to manage project's database
const Project = require('../models/project');

// Make public to use the next methods
module.exports = {

	/* 
		INFORMATION ABOUT PARAMS:
		req: request sended by the client
		res: response which the server is gonna send after finish this method.
		next: function handle the control to the next  middlewar 
	*/

	/*
	Index Method
		return an array of jsons with all the projects 
	*/
	index: (req, res, next) => {
		// Asking for all projects to the database
		Project.find({}) 
			.then(projects => {
				// After the data is fetch then return a 200 and the data
				res.status(200).json(projects);
			})
			.catch(err => {
				// Send the error to the error handler middleware
				next(err);
			})
	},

	/*
	newProject Method
		It recives a project in the req and creates it in the database.
		Also the method return a json with the new project.
	*/
	newProject: (req, res, next) => {

		// Create a project object with the json from the validation middleware
		const newProject = new Project(req.value.body);
		// Save it in the database
		newProject.save()
			.then(project => {
				// After saving it  return a 201 and the project created
				res.status(201).json(project);
			})
			.catch(err => {
				// Send the error to the error handler middleware
				next(err);
			}) 
	},

	/*
	getProject Method
		It recives a project id in the request params.
		After that returns the project found or an error.
	*/
	getProject: (req, res, next) => {

		// Get the projects id sended by the validation helper.
		const projectId = req.value.params.projectId;
		// Look for the project by its ID
		Project.findById(projectId)
			.then(project => {
				// Return it if it was found.
				res.status(200).json(project);
			})
			.catch(err => {
				// Send the error to the error handler middleware
				next(err);
			})
	},

	/*
	replaceProject Method
		It recives a project id in the request params and a project in the 
		request body.
		After that it replaces the whole project object.
	*/
	replaceProject: (req, res, next) => {
		// Get the projects id sended by the validation helper
		const projectId = req.value.params.projectId;

		// Get the json sended through the body and already validated
		const newProject = req.value.body;

		// Find in the database a project by Id, if found then replace with the
		// json sended as the new project
		Project.findByIdAndUpdate(projectId, newProject)
			.then(result => {
				// If everthing ok send 200 and a succesfull true json
				res.status(200).json({success:true});
			})
			.catch(err => {
				// Send the error to the error handler middleware
				next(err);
			})
	},

	/*
	updateProject Method
		It recives a project id in the request params and project fields in the 
		request body.
		After the project is found the method change its fields, but just 
		the ones sended in the body params.
	*/
	updateProject: (req, res, next) => {
		// Id from the project whos gonna get some of its fields changed
		const projectId = req.value.params.projectId;
		// The fields to change in the project
		const newProject = req.value.body;

		// Find in the database a project by Id and update the fields.
		Project.findByIdAndUpdate(projectId, newProject)
			.then(result => {
				// If everyhing ok send 200 and a success message.
				res.status(200).json({success:true});
			})
			.catch(err => {
				// Send the error to the error handler middleware
				next(err);
			})
	},

	/*
	getProjectAssociatedProjects Method
		This function returns the projects associated with a specific project
		It recibes a project id through params
	*/
	getProjectAssociatedProjects: (req, res, next) => {
		// Id from the parent project
		const projectId = req.value.params.projectId;

		// Get all ids for the associated projects and "build" its objects
		Project.findById(projectId).populate('projects_associated')
			.then(project => {
				// Send the project's associated projects in an array
				res.status(200).json(project.projects_associated);
			})
			.catch(err => {
				// Send the error to the error handler middleware
				next(err);
			})
	},

	/*
	newProjectAssociatedProjects Method
		This function push projects to the associated project of a specific project
		It recibes a project id through params
	*/
	newProjectAssociatedProjects: (req, res, next) => {
		// Get all ids send to "associate" with the main project
		const child_projectsId = req.body.projects;
		// Get the id of the main project
		const parent_projectId = req.params.projectId;
		// An array whit all project objects 
		var projects_associated = [];

		// This loop is made to validate all projects before saving
		for(var i = 0; i < child_projectsId.length; i++){
			// Find project by id from the ids sended to "associate"
			Project.findById(child_projectsId[i]).
				then(project => {
					// If the project exist push it to the array with project objects
					projects_associated.push(project);
				})
				.catch(err => {
					// Send the error to the error handler middleware
					// This first error inside the function it could be mainly for
					// sending a non existen project to associated with
					next(err);
				});
		}

		// After all projects have been find we can start to add em to
		// the parent project

		// Find the main project
		Project.findById(parent_projectId).
			then(project => {
				// Loop through every project found to associate
				for(var j = 0; j < projects_associated.length ; j++){
					// Push the project to the main project's associates
					project.projects_associated.push(projects_associated[j]);
				}

				// Save the main project and return a success message
				project.save();
				res.status(201).json({success: true});
				
			})
			.catch(err => {
				// Send the error to the error handler middleware
				// This second error could be mainly for the id of the main project 
				next(err);
			})


	}
};