# PSL Tracker

>Warning: Make sure you're using the latest version of Node.js and NPM

### Quick start

```bash
# clone our repo
$ git clone https://bitbucket.org/pslpraxis/p-tracker 

# change directory to your app
$ cd frontend

# install the dependencies with npm
$ npm install

# start the server
$ npm start
```
go to [http://localhost:4000](http://localhost:4000) in your browser.
