import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../shared';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'my-hard-skill-detail',
  templateUrl: './hardSkillDetail.component.html',
  styleUrls: ['./hardSkillDetail.component.scss']
})
export class HardSkillDetailComponent implements OnInit {

  protected hard_skill: any; // The hardSkill who match the id send in the params
  private sub: any; // Object used to handle the params
  private id: number; // Id passed in the params

  // Recives 2 params, a api object to ask for info to the backend
  // And a route to get the params send through the URL
  constructor(protected api: EmployeeService, private route: ActivatedRoute) {

    // This stores the id send on the URL to the id variable and parse it to number
    this.sub = this.route.params.subscribe(params => {
       this.id = +params['id']; // (+) converts string 'id' to a number
    });

    // This call an API function to ask for a specific hard skill
    // And stores it in the hard_skill object
    api.getHardSkill(this.id).subscribe((hard_skill) => {
            this.hard_skill = hard_skill;
        });
  }

  ngOnInit() {
    
  }

}
