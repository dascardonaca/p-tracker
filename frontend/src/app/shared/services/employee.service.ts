import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class EmployeeService {

  // Resolve HTTP using the constructor
  constructor(private http: Http) { }

  public getProjects(): Observable<Response & any> {

    // ...using get request
    return this.http.get('/projects');

  }

  public getProject(id): Observable<Response & any> {

    // ...using get request
    return this.http.get('/projects/' + id);

  }

  public getEmployee(id): Observable<Response & any> {

    // ...using get request
    return this.http.get('/employees/' + id);

  }

  public getHardSkills(employee_id): Observable<Response & any> {

    // ...using get request
    return this.http.get('/hardSkills/' + employee_id);

  }

  public getHardSkill(id): Observable<Response & any> {

    // ...using get request
    return this.http.get('/hardSkill/' + id);

  }

  public getSoftSkills(employee_id): Observable<Response & any> {

    // ...using get request
    return this.http.get('/softSkills/' + employee_id);

  }

  public getSoftSkill(id): Observable<Response & any> {

    // ...using get request
    return this.http.get('/softSkill/' + id);

  }

  public getSoftSkillByEmployeeAndProject(employeeId, projectId): Observable<Response & any> {

  // ...using get request
  return this.http.get('/employees/' + employeeId + '/' + projectId + '/softskill');

}

  public getHardSkillByEmployeeAndProject(employeeId, projectId): Observable<Response & any> {

    // ...using get request
    return this.http.get('/employees/' + employeeId + '/' + projectId + '/hardskill');

  }

  public getAllSoftSkills(): Observable<Response & any> {

    // ...using get request
    return this.http.get('/softskills/');

  }

  public getAllHardSkills(): Observable<Response & any> {

    // ...using get request
    return this.http.get('/hardskills/');

  }

}
