// Module to made validations
const Joi = require('joi');

// Make public to use the next methods
module.exports = {
	validateParam: (schema, name) => {
		// Returns a function 
		return (req, res, next) => {
			// Use joi module to compare the param sended agains a schema
			const result = Joi.validate({ param: req['params'][name]}, schema);
			if (result.error){
				// Error handling, the param attribute didnt match the schema
				return res.status(400).json(result.error);
			} else {
				// The value attribute is made to be sure in the controller that the 
				// fields send by the req.params have been validated
				if(!req.value)
					req.value = {};

				// value.params may be > 1  if we're checking 2 attributes at time.
				if(!req.value['params'])
					req.value['params'] = {}

				// Inside value[params] send key value with the name of the attribute
				// and the value validated.
				req.value['params'][name] = result.value.param;

				// Green across the board, send to controller.
				next();
			}
		}
	},

	validateBody: (schema) => {
		return (req, res, next) => {
			const result = Joi.validate(req.body, schema);

			if(result.error){
				return res.status(400).json(result.error);
			} else {
				if(!req.value)
					req.value = {};

				if(!req.value['body'])
					req.value['body'] = {};

				req.value['body'] = result.value;
				next();
			}
		}
	},

	schemas: {
		// Used for all but patch request
		projectSchema: Joi.object().keys({
			name: Joi.string().required(),
			service_type: Joi.boolean(),
			vertical_product: Joi.string(),
			other_vertical_product: Joi.string(),
			SO_client: Joi.string(),
			languages_and_frameworks: Joi.array().items(Joi.string()),
			tools_and_libraries: Joi.array().items(Joi.string()),
			plataforms_and_tools: Joi.array().items(Joi.string()),
			techniques_and_practices: Joi.array().items(Joi.string()),
			natl_intl: Joi.string(),
			language: Joi.string(),
			JIRA_PSL: Joi.boolean(),
			project_management_tool: Joi.string(),
			repository_tool: Joi.string()
		}),

		// Used for patch request to handle all permutation of attributes
		projectOptionalSchema: Joi.object().keys({
			name: Joi.string(),
			service_type: Joi.boolean(),
			vertical_product: Joi.string(),
			other_vertical_product: Joi.string(),
			SO_client: Joi.string(),
			languages_and_frameworks: Joi.array().items(Joi.string()),
			tools_and_libraries: Joi.array().items(Joi.string()),
			plataforms_and_tools: Joi.array().items(Joi.string()),
			techniques_and_practices: Joi.array().items(Joi.string()),
			natl_intl: Joi.string(),
			language: Joi.string(),
			JIRA_PSL: Joi.boolean(),
			project_management_tool: Joi.string(),
			repository_tool: Joi.string()
		}),

		idSchema: Joi.object().keys({
			param: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
		})
	}
}