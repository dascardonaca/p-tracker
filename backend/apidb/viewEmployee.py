from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

from . import mongoEmployee

def index(request):
    return JsonResponse({'Root': 'Api'})

def employees(request):
	return JsonResponse(mongoEmployee.getEmployees(), safe=False)

def employeeId(request, id):
	return JsonResponse(mongoEmployee.getEmployeId(id))

def employeeName(request, name):
	return JsonResponse(mongoEmployee.getEmployeName(name))

