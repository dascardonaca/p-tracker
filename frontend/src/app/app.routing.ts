import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { SearchBasicComponent } from './searchEngine/searchBasic/searchBasic.component';
import { HardSkillDetailComponent } from './hardSkillDetail/hardSkillDetail.component';
import { SkillsComponent } from './skills/skills.component';

import { AdvancedSearchComponent } from './searchEngine/advancedSearch/advancedSearch.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'skills/:id', component: SkillsComponent },
  { path: 'hard-skill-detail', component: HardSkillDetailComponent },
  { path: 'search-basic', component: SearchBasicComponent },
  { path: 'advanced-search', component: AdvancedSearchComponent }
];

export const routing = RouterModule.forRoot(routes);
