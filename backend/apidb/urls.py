from django.conf.urls import url

from . import views
from . import viewHardSkill
from . import viewEmployee
from . import viewProject

urlpatterns = [

    url(r'^$', views.index, name='index'),
    url(r'^hardSkills/(?P<employee_id>[0-9]{1,})', views.hardSkills, name='hardSkills'),
    url(r'^hardSkill/(?P<id>[0-9]{1,})', views.hardSkill, name='hardSkill'),
    #url(r'^hardSkills', viewHardSkill.hardSkills, name='hardSkills'),
    #url(r'^hardSkill/(?P<id>[0-9]{1,})', viewHardSkill.hardSkill, name='hardSkill'),
    url(r'^employees', viewEmployee.employees, name='employees'),
    url(r'^employee/(?P<id>[0-9]{1,})', viewEmployee.employeeId, name='employeeId'),
    url(r'^employee/(?P<name>[\w\-]+)', viewEmployee.employeeName, name='employeeName'),
    url(r'^projects', viewProject.projects, name='projects'),
    url(r'^project/(?P<id>[0-9]{1,})', viewProject.projectId, name='projectId'),
    url(r'^project/(?P<name>[\w\-]+)', viewProject.projectName, name='projectName')
]
