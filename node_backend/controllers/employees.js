"use strict"
const Employee = require('../models/employee');
const Project = require('../models/project');
const Hardskill = require('../models/hardskill');
const Softskill = require('../models/softskill');
const Hardevaluation = require('../models/hardevaluation');
const Softevaluation = require('../models/softevaluation');

module.exports={

  // Index
  getEmployee:(req,res,next)=>{
    Employee.find({})
      .then(employees =>{
        res.status(200).json(employees);
      })
      .catch(err =>
        next(err));
  },

  // Get with employeeId in params
  getEmployeeId:(req, res, next) =>{
    let employeeId = req.params.employeeId;
    Employee.findById(employeeId)
      .then(employee =>{
        res.status(200).json(employee);
      })
      .catch(err =>{
        next(err);
      })
  },

  // Post with no params
  saveEmployee:(req,res,next)=>{
    const newEmployee = new Employee(req.body);

    newEmployee.save()
      .then(employeeStored =>{
        res.status(201).json({employee: employeeStored});
      })
      .catch(err => {
        next(err);
      })
  },

  // Put with employee id in params and new employee on body
  replaceEmployee: (req, res, next)=>{
    const employeeId =  req.params.employeeId;
    const newEmployee = req.body;

    Employee.findByIdAndUpdate(employeeId, newEmployee)
      .then(result => {
        res.status(200).json({success: true});
      })
      .catch(err =>{
        next(err);
      })
  },

  updateEmployee: (req, res, next)=>{
    let employeeId =  req.params.employeeId;
    let update = req.body;

    Employee.findByIdAndUpdate(employeeId, update)
      .then(result => {
        res.status(200).json({success: true});
      })
      .catch(err =>{
        next(err);
      })
  },

  deleteEmployee:(req,res,next)=>{
    let employeeId = req.params.employeeId;
    Employee.remove({_id:employeeId})
      .then(result =>{
          res.status(200).json({success: true});
      })
      .catch(err=>{
        next(err);
      })
  },

  getProjects: (req, res, next) => {
    const employeeId = req.params.employeeId;

    Employee.findById(employeeId).populate('projects')
      .then(employee =>{
        res.status(200).json(employee.projects);
      })
      .catch(err =>{
        next(err);
      })
  },

  // Insert projects already in the system to a employee
  newProjects: (req, res,next) => {
    // Ids from the projects
    const new_projectsId = req.body.projects;
    // Id of the employee to modify
    const employeeId = req.params.employeeId;
    // Array with the projects
    var projects = [];

    // Loop the projects id array
    for(var i = 0; i< new_projectsId.length; i++){
      // Find a project with a project Id
      Project.findById(new_projectsId[i]).
        then(project => {
          // If the project exist add it to the projects array
          projects.push(project);
        })
        .catch(err => {
          // If a project doesnt exit dont save anything and send a error
          next(err);
        });
    }

    //find the employee for id
    Employee.findById(employeeId).
      then(employee => {
        // Insert the projects to the employee
        for(var j = 0; j < projects.length; j++){
          // In the employee attribute projects push the project
          employee.projects.push(projects[j]);
        }

        // Employee with projects... done *Gordon Ramsay accent*
        employee.save()
        res.status(201).json({success: true});

      })
      .catch(err => {
        // If the employee doesn exist dont save anything and send error
        next(err)
      })
  },

  //get the hardskill of the employee
  getHardskills: (req, res, next) => {
    const employeeId = req.params.employeeId;

    Employee.findById(employeeId).populate('hardskills')
      .then(employee =>{
        res.status(200).json(employee.hardskills);
      })
      .catch(err =>{
        next(err);
      })
  },

  //insert a new hardskill to employee
  newHardskills: (req, res, next) => {
    const new_hardskillId = req.body.hardskills;
    const employeeId = req.params.employeeId;
    var hardskills = [];

    //loop for insert the new hardskills to employee
    for(var i = 0; i < new_hardskillId.length; i++){
      Hardskill.findById(new_hardskillId[i]).
        then(object => {
          hardskills.push(object);
        })
        .catch(err => {
          next(err);
        })
    }
    //find the employee for ID
    Employee.findById(employeeId).
      then(employee => {

        for(var j = 0; j < hardskills.length; j++){

          employee.hardskills.push(hardskills[j]);
        }
        //save the new hardskills to Employee
        employee.save()
        res.status(201).json({success: true});

      })
      .catch(err => {
        next(err)
      })

  },

  // Skills by Employee
  getSoftskills: (req, res, next) => {
    const employeeId = req.params.employeeId;

    Employee.findById(employeeId).populate('softskills')
      .then(employee =>{
        res.status(200).json(employee.softskills);
      })
      .catch(err =>{
        next(err);
      })
  },

  //Insert new softskills to employee
  newSoftskills: (req, res, next) => {
    const new_softskillId = req.body.softskills;
    const employeeId = req.params.employeeId;
    var softskills = [];

    for(var i = 0; i < new_softskillId.length; i++){
      Softskill.findById(new_softskillId[i]).
        then(object => {
          softskills.push(object);
        })
        .catch(err => {
          next(err);
        })
    }

    Employee.findById(employeeId).
      then(employee => {

        for(var j = 0; j < softskills.length; j++){

          employee.softskills.push(softskills[j]);
        }

        employee.save()
        res.status(201).json({success: true});

      })
      .catch(err => {
        next(err)
      })
  },


  // Skills by Employee and Project
  getHardEvaluation: (req, res, next) => {
    const employeeId = req.params.employeeId;
    const projectId = req.params.projectId;

    var hardskills = Hardevaluation.find({ project : projectId})
      .then(hardskills =>{
        res.status(200).json(hardskills);
      })
      .catch(err =>
        next(err));
  },

  newHardEvaluation: (req, res, next) => {
    const employeeId = req.params.employeeId;
    const projectId = req.params.projectId;
    const hardskillId = req.body.hardskill;
    const score = req.body.score;

    const  newHardevaluation = new Hardevaluation({
      employee: employeeId,
      project: projectId,
      hardskill: hardskillId,
      score: score
    });

    newHardevaluation.save()
      .then(hardevaluation =>{
        res.status(200).json(hardevaluation);
      })
      .catch(err => {
        next(err);
      })
  },

  getSoftEvaluation: (req, res, next) => {
    const employeeId = req.params.employeeId;
    const projectId = req.params.projectId;

    var softskills = Softevaluation.find({ project : projectId})
      .then(softskills =>{
        res.status(200).json(softskills);
      })
      .catch(err =>
        next(err));
  },

  newSoftEvaluation: (req, res, next) => {
    const employeeId = req.params.employeeId;
    const projectId = req.params.projectId;
    const softskillId = req.body.softskill;
    const score = req.body.score;

    const  newSoftevaluation = new Softevaluation({
      employee: employeeId,
      project: projectId,
      softskill: softskillId,
      score: score
    });

    newSoftevaluation.save()
      .then(softevaluation =>{
        res.status(200).json(softevaluation);
      })
      .catch(err => {
        next(err);
      })
  }

}
