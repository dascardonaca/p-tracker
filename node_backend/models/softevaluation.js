const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const softevaluationSchema = new Schema({

	employee: {
		type: Schema.Types.ObjectId,
	},

	project: {
		type: Schema.Types.ObjectId,
		ref: 'project'
	},

	softskill: {
		type: Schema.Types.ObjectId,
		ref: 'softskill'
	},

	score: Number

});

const Softevaluation = mongoose.model('softevaluation', softevaluationSchema);

module.exports = Softevaluation;