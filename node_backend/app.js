// Importing modules
const express = require('express');
const logger = require('morgan');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
var cors = require('cors');
var path = require('path');



// What tipe of promisses mongoose is gonna use
mongoose.Promise = global.Promise;

// Conection with the database 'protocol/place/name'
mongoose.connect('mongodb://localhost/ptracker');

// Iniciate the application
const app = express();

// config use cors
var corsOptions = {
	origin: 'http://localhost:4000',
	optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}
app.use(cors())

// Routes

// Importing routes
const projects = require('./routes/projects');
const hardskills = require('./routes/hardskills');
const employees = require('./routes/employees');
const softskills = require('./routes/softskills');
// Middlewares

// Morgan generates logs automatically on any requests made
app.use(logger('dev'));
app.use(express.static(__dirname + '/public'))
// Bodyparser to parse the body from the html request to json
app.use(bodyParser.json());

// Using projects routes under the /projects domain
app.use('/projects', projects);

// Using hardkills routes inther the /hardskills domain
app.use('/hardskills', hardskills);

// Using employees routes under the /employee domain
app.use('/employees', employees);

// Using employees routes under the /softskills domain
app.use('/softskills', softskills);

// Catch 404 Errors and forward them to error handler
app.use((req, res, next) => {
	const err = new Error('Not Found');
	err.status = 404;
	next(err);
});




// Error Handler function
app.use((err, req, res, next) => {
	
	// Create an error if we're in development enviroment
	const error = app.get('env') === 'development' ? err : {};
	
	// If there is no status, pass error 500
	const status = err.status || 500;
	
	//Respond to client
	res.status(status).json({
		error: {
			message: error.message
		}
	});
	
	//Respond to ourselves
	console.error(err);
});

// Start the server

// Watch if there is a port enviroment variable and use it
// Or if there isnt any, use port 3000
const port = app.get('port') || 3000;

// Setting the port and the start message for the application
app.listen(port, () => console.log('Server is listening on port ' + port));

module.exports = app;