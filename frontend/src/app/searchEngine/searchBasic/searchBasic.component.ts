import { Component } from '@angular/core';

import { ApiService } from '../../shared';
import { ProjectService } from '../../shared';
import { IEmployee } from '../../shared';




/**
 * Component is used for the view  of search basic by employees and projects 
 */
@Component({
    selector: 'my-search-basic',
    templateUrl: './searchBasic.component.html',
    styleUrls: ['./searchBasic.component.scss']
})
export class SearchBasicComponent {
    /** employees list  */
    protected employees: Array<IEmployee>;
    /** projects  */
    protected projects: Array<any>;
    /** search basic  */
    protected searchBasic: string;
    /** result list */
    protected resultList: Array<any> = [];
    /** filter  */
    protected filter: any;
    constructor(protected api: ApiService, protected projectService: ProjectService) {
        this.projectService.getProjects().subscribe((projects) => {
            this.projects = projects;
        });
        api.getEmployees().subscribe((employees) => {
            this.employees = employees;
        });
        this.filter = '{"list": "employees", "attr": "name"}';
    }

    public filterSearch(): void {
        this.filter = JSON.parse(this.filter);
        this.resultList = [];
        this.resultList = this[this.filter.list].filter(item =>
            item[this.filter.attr].toLowerCase().indexOf(this.searchBasic.toLowerCase()) !== -1
        );
    }
}
