from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

from . import mongoHardSkill
from . import mongoEmployee

def index(request):
    return JsonResponse({'Root': 'Api'})

def hardSkills(request):
	return JsonResponse(mongoHardSkill.getHardSkills(), safe=False)

def hardSkill(request, id):
	return JsonResponse(mongoHardSkill.getHardSkill(id))
