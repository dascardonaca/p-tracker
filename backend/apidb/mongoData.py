from pymongo import MongoClient
from bson import json_util
import json

#Connection to the Mongo Server
mongoClient = MongoClient('localhost',27017)

#Accessing the database
db = mongoClient.ptracker


def getHardSkills(employee_id):
	#Get a collection
	collection = db.hardSkillsList
	cursor = collection.find({'employee_id': int(employee_id)}, {'_id': False})
	json_docs = []

	for doc in cursor:
		json_docs.append(doc)

	return json_docs

def getHardSkill(id):
	#Get a collection
	collection = db.competencies
	cursor = collection.find_one({'id': int(id)}, {'_id': False})

	return cursor