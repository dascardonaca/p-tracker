import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';


@Injectable()
export class ProjectService {

    // Resolve HTTP using the constructor
    constructor(private http: Http) { }

    // Fetch all existing comments
    public getProjects(): Observable<Response & any> {
        // ...using get request
        return this.http.get('/projects');
    }

    public getProjectsHardskills(): Observable<Response & any> {
        /*db.projects.find({"employees.hard_skills.skill_name":"Git"},{"project_name":1})  */      
        return this.http.get('/projects');
    }

}
