import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../shared';

@Component({
  selector: 'my-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  // Used to store all values returned by the api
  protected projects: Array<any>;

  // Recives one object api to ask for data to the backend
  constructor(protected api: EmployeeService) {
    api.getProjects().subscribe((projects) => {
      this.projects = projects;
    });
  }

  ngOnInit() {
  }

}


