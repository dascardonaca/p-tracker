const mongoose = require ("mongoose");
const Schema = mongoose.Schema;

const softskillSchema = new Schema({

	name:{
		type: String,
		enum :[
					"etica",
					"responsabilidad",
					"empatia",
					"sociabilidad",
					"facilidad de comunicacion",
					"escucha activa",
					"creatividad",
					"optimizacion del tiempo",
					"espiritu de servicio",
					"asertividad",
					"respeto a las opiniones"
					]
	      }
})

const Softskill = mongoose.model('softskill', softskillSchema);

module.exports = Softskill;
