const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const hardskillSchema = new Schema({

	name: String,
	description: String,
	type: String,
	projects: [{
		type: Schema.Types.ObjectId,
		ref: 'project'
	}]
});
/*
  isNew :Boolean,
	descriptionRadar : String
*/
const Hardskill = mongoose.model('hardskill', hardskillSchema);

module.exports = Hardskill;
