process.env.NODE_ENV = 'test';

const mongoose = require("mongoose");
const Project = require('../models/project');

const agent = require('superagent-promise')(require('superagent'), Promise);
const statusCode = require('http-status-codes');
const chai = require('chai');
const chaiHttp = require('chai-http');
const _ = require('lodash');
const app = require('../app');

const expect = chai.expect;
const should = chai.should();

const ProjectsController = require('../controllers/projects');

chai.use(chaiHttp);

describe('Projects', () => {

	beforeEach((done) => { //Before each test we empty the database
		Project.remove({}, (err) => { 
			done();         
		});     
	});

	describe('GET /project', () => {
		it('it should GET all the projects', (done) => {
			chai.request(app)
				.get('/projects')
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.be.a('array');
					res.body.length.should.be.eql(0);
					done();
				});
		});
	});

	describe('GET /project/:projectId', () => {

		it('it should GET a project by the given Id', (done) => {
			const project = new Project({name: "Test project"});
			project.save((err, project) => {
				chai.request(app)
				.get('/projects/' + project._id)
				.send(project)
				.end((err, res) => {
					res.should.have.status(200);
					res.body.should.have.property('name');
					res.body.should.have.property('_id');
					res.body.should.be.a('object');
					done();
				});
			});
		});

		it('it should GET a joi validation with a wrong Id', (done) => {
			const wrongId = "NO_AN_ACTUAL_ID";

			const project = new Project({name: "Test project"});

			project.save((err, project) => {
				chai.request(app)
				.get('/projects/' + wrongId)
				.send(project)
				.end((err, res) => {
					// Error status send by joi module
					res.should.have.status(400);

					// Validation from Helper should be here
					res.body.should.have.property('isJoi');
					res.body.should.be.a('object');
					done();
				});
			});
		});

		it('it should GET a null with a correct but non existent Id', (done) => {
			const wrongId = "000000000000000000000000";
			
			const project = new Project({name: "Test project"});

			project.save((err, project) => {
				chai.request(app)
				.get('/projects/' + wrongId)
				.send(project)
				.end((err, res) => {
					res.should.have.status(200);
					should.equal(res.body, null);
					done();
				});
			});
		});

	});

	describe('/POST project', () => {
		it('it should not POST empty project', (done) => {
			const project = {};

			chai.request(app)
				.post('/projects')
				.send(project)
				.end((err, res) => {
					// Error status send by joi module
					res.should.have.status(400);
					
					// Validation from Helper should be here
					res.body.should.have.property('isJoi');
					res.body.should.be.a('object');
					done();
				});
		});

		it('it should POST project', (done) => {
			const project = {
				name: "Test"
			};

			chai.request(app)
				.post('/projects')
				.send(project)
				.end((err, res) => {
					// Error status send by joi module
					res.should.have.status(201);
					
					// Validation from Helper should be here
					res.body.should.have.property('name');
					res.body.should.have.property('_id');
					res.body.should.be.a('object');
					done();
				});
		});

	});

	describe('/PUT/:id project', () => {
      it('it should UPDATE a project given the id', (done) => {
        const project = new Project({name: "Castelvania"});
        project.save((err, project) => {
                chai.request(app)
                .put('/projects/' + project._id)
                .send({name: "Dracula"})
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('success').eql(true);
                  done();
                });
          });
      });
  });


});
