from django.shortcuts import render
from django.http import HttpResponse, JsonResponse

from . import mongoData
import json



def index(request):
    return JsonResponse({'Root': 'Api'})

def hardSkills(request, employee_id):
  return JsonResponse(mongoData.getHardSkills(employee_id), safe=False)

def hardSkill(request, id):
	return JsonResponse(mongoData.getHardSkill(id))

def employees(request):
  data = [
      {
        'name': 'Francisco Gomez',
        'project': 'Avianca',
        'role': 'Scrum Master'
      },
      {
        'name': 'Luisa Toro',
        'project': 'Avianca',
        'role': 'Developer'
      },
      {
        'name': 'Adriana Restrepo',
        'project': 'Here',
        'role': 'Tester'
      },
      {
        'name': 'Emmanuel Castaño',
        'project': 'Here',
        'role': 'Developer'
      },
      {
        'name': 'Isabella Velez',
        'project': 'Cadreon',
        'role': 'Scrum Master'
      },
      {
        'name': 'Paula Gomez',
        'project': 'Novaventa',
        'role': 'Developer'
      },
      {
        'name': 'Ana Sofia Perez',
        'project': 'Novaventa',
        'role': 'Product Owner'
      }
    ]
  return JsonResponse(data, safe=False)
