const express = require('express');
const router = express.Router();

const SoftskillController = require('../controllers/softskills');

router.route('/')
	.get(SoftskillController.index)
	.post(SoftskillController.newSoftskill);

router.route('/:softskillId')
	.get(SoftskillController.getSoftskill)
	.put(SoftskillController.replaceSoftskill)
	.patch(SoftskillController.updateSoftskill)
	.delete(SoftskillController.deleteSoftskill);


// Make public the router object with the softskills resources.
module.exports = router;