const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const hardevaluationSchema = new Schema({

	employee: {
		type: Schema.Types.ObjectId,
	},

	project: {
		type: Schema.Types.ObjectId,
		ref: 'project'
	},

	hardskill: {
		type: Schema.Types.ObjectId,
		ref: 'hardskill'
	},

	score: Number

});

const Hardevaluation = mongoose.model('hardevaluation', hardevaluationSchema);

module.exports = Hardevaluation;