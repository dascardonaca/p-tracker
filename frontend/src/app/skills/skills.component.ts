import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../shared';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'my-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {

  protected project: any;
  protected hard_skills: Array<any>; // Used to store all values returned by the api
  protected soft_skills: Array<any>; // Used to store all values returned by the api
  private sub: any; // Object used to handle the params
  private id: number; // Id passed in the params
  private employee_id: string = '59fbd741bac25412bcd50399'; // Tmp Id #TODO
  // Used to store all values returned by the api
  protected projects: Array<any>;
  protected employee: any;

  protected all_hard_skills: Array<any>;
  protected all_soft_skills: Array<any>;

  // Recives 2 params, a api object to ask for info to the backend
  // And a route to get the params send through the URL

  constructor(protected api: EmployeeService, private route: ActivatedRoute) {

    // This stores the id send on the URL to the id variable and parse it to number
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      this.loadData();
    });


    api.getProjects().subscribe((projects) => {
      this.projects = projects;
    });

    api.getEmployee(this.employee_id).subscribe((employee) => {
      this.employee = employee;
    });

    this.api.getAllHardSkills().subscribe((hard_skills) => {
      this.all_hard_skills = hard_skills;
    });

    this.api.getAllSoftSkills().subscribe((soft_skills) => {
      this.all_soft_skills = soft_skills;
    });

  }


  ngOnInit() {
  }

  loadData() {
    this.api.getProject(this.id).subscribe((project) => {
      this.project = project;
    });

    this.api.getHardSkillByEmployeeAndProject(this.employee_id, this.id).subscribe((hard_skills) => {
      this.hard_skills = hard_skills;
    });

    this.api.getSoftSkillByEmployeeAndProject(this.employee_id, this.id).subscribe((soft_skills) => {
      this.soft_skills = soft_skills;
    });
  }

  skillById(id, list) {
    if (list !== undefined) {
      return list.filter(skill => id === skill._id)[0].name;
    } else {
      return '';
    }
  }

  skillTypeById(id, list) {
    if (list !== undefined) {
      return list.filter(skill => id === skill._id)[0].type;
    } else {
      return '';
    }
  }
}
