const express = require('express');
const router = express.Router();

const HardskillsController = require('../controllers/hardskills');

router.route('/')
	.get(HardskillsController.index)
	.post(HardskillsController.newHardskill);

router.route('/:hardskillId')
	.get(HardskillsController.getHardskill)
	.put(HardskillsController.replaceHardskill);

// Make public the router object with the hardskills resources.
module.exports = router;
