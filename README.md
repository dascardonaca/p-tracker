# README #

## Mongo instalation guide

Be sure you have the mongo bin path C:\Program Files\MongoDB\Server\3.4\bin in the system enviroment.

Execute "mongod --config mongodb.config" inside the mongodb folder

Execute "npm start" inside the frondend folder

Execute "python manage.py runserver" inside the backend folder

## Mongo insert to ptracker db, hardSkillsList collection

db.hardSkillsList.insert(
	{
		employee_id: 101, 
	 	employee_name: "Jose", 
	 	employee_roll: "Developer",
	 	employee_projects:
	 		[
			 	{
			 		project_id: 100213,
			 		project_name:"Projecto Totto", 
			 		hardskills:[
			 			{hard_skill_id:3, hard_skill_name: "Java", type: "Languages & Frameworks"}, 
			 			{hard_skill_id:4, hard_skill_name: "C#", type: "Languages & Frameworks"}
			 		]
		 		},
		 		{
		 			project_id: 879675,
			 		project_name:"American Web", 
			 		hardskills:[
			 			{hard_skill_id:1, hard_skill_name: "Python", type: "Tecnologias y Prácticas"}, 
			 			{hard_skill_id:2, hard_skill_name: "C#", type: "Languages & Frameworks"}
			 		]
			 	}, 
			 	{
			 		project_id: 654280,
			 		project_name:"Projecto Totto", 
			 		hardskills:[
			 			{hard_skill_id:6, hard_skill_name: "Spring", type: "Languages & Frameworks" }, 
			 			{hard_skill_id:5, hard_skill_name: "TDD", type: "Tecnologias y Prácticas"}
			 		]
		 		}
	 		]
	}
)