from pymongo import MongoClient
from bson import json_util
import json
from bson.json_util import dumps

import pprint

#Connection to the Mongo Server
mongoClient = MongoClient('localhost',27017)

#Accessing the database
db = mongoClient.ptracker

#Get a collection
collection = db.competencies
#collection = db.employees

def getHardSkills():
	cursor1 = collection.find()
	page_sanitized = json.loads(dumps(cursor1))
	return page_sanitized

def getHardSkill(id):
	id=int(id)

	for cursor2 in collection.find({"_id":id}):
		data=dumps(cursor2)

	page_sanitized = json.loads(data)
	return page_sanitized







