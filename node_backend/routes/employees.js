const express = require('express');
const router = express.Router();

const EmployeeController = require('../controllers/employees')

router.route('/')

  .get(EmployeeController.getEmployee)
  .post(EmployeeController.saveEmployee);

router.route('/:employeeId')
	.get(EmployeeController.getEmployeeId)
	.put(EmployeeController.replaceEmployee)
	.patch(EmployeeController.updateEmployee)
	.delete(EmployeeController.deleteEmployee);

router.route('/:employeeId/projects/')
	.get(EmployeeController.getProjects)
	.post(EmployeeController.newProjects);

router.route('/:employeeId/hardskills/')
	.get(EmployeeController.getHardskills)
	.post(EmployeeController.newHardskills);

router.route('/:employeeId/softskills/')
	.get(EmployeeController.getSoftskills)
	.post(EmployeeController.newSoftskills);

router.route('/:employeeId/:projectId/hardskill')
	.get(EmployeeController.getHardEvaluation)
	.post(EmployeeController.newHardEvaluation);

router.route('/:employeeId/:projectiD/softskill')
	.get(EmployeeController.getSoftEvaluation)
	.post(EmployeeController.newSoftEvaluation);

module.exports = router;
